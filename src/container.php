<?php
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use Lurker\ResourceWatcher;
use Monolog\Logger;
use Symfony\Bridge\Monolog\Handler\ConsoleHandler;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Tui\Rusty\Handler\ImagineHandler;

$di = new Pimple;

/**
 * console.output: an instance of Symfony\Component\Console\Output\ConsoleOutputInterface
 */
$di['console.output'] = $di->share(function () use ($di) {
    return new ConsoleOutput;
});

/**
 * logger: a PSR-3 compliant logger. Extend this to add log handlers (files, raven, greylog, etc)
 */
$di['logger'] = $di->share(function () use ($di) {
    $logger = new Logger('rusty');
    $logger->pushHandler(new ConsoleHandler($di['console.output']));

    return $logger;
});

/**
 * dispatcher: an instance of Symfony\Component\EventDispatcher\EventDispatcherInterface
 */
$di['dispatcher'] = $di->share(function () use ($di) {
    return new EventDispatcher;
});

/**
 * watcher: something like Lurker's ResourceWatcher
 */
$di['watcher'] = $di->share(function () use ($di) {
    $watcher = new ResourceWatcher(null, $di['dispatcher']);

    return $watcher;
});

/**
 * Image handler: instance of Tui\Rusty\Handler\HandlerInterface
 */
$di['image.handler'] = $di->share(function () use ($di) {
    $handler = new ImagineHandler($di['imagine']);
    $handler->setLogger($di['logger']);
    $handler->setVariants($di['image.sizes']);

    return $handler;
});


/**
 * imagine: instance of Imagine\Image\ImagineInterface
 */
$di['imagine'] = $di->share(function () use ($di) {
    if (class_exists('Imagick')) {
        $imagine = new Imagine\Imagick\Imagine();
    } elseif (class_exists('GMagick')) {
        $imagine = new Imagine\Gmagick\Imagine();
    } elseif (function_exists('imagecreate')) {
        $imagine = new Imagine\GD\Imagine();
    } else {
        throw new \RuntimeException('Neither IMagick nor GD found');
    }

    return $imagine;
});


// CONFIGURATION

/**
 * Image sizes. 
 * Height is always according to original aspect ratio
 * Quality applies to JPEGs only
 * TODO: these should be user-configurable
 */
$di['image.sizes'] = [
    ''  => ['width' => 320*2, 'quality' => 80],
    'medium' => ['width' => 640*2, 'quality' => 80],
    'large'  => ['width' => 960*2, 'quality' => 80],
    'huge'   => ['width' => 1440*2, 'quality' => 80],
];

return $di;
