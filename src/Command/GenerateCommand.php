<?php
namespace Tui\Rusty\Command;

use League\Flysystem\Adapter\Local as Adapter;
use League\Flysystem\Filesystem;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Tui\Rusty\Handler\HandlerInterface;

class GenerateCommand extends Command
{
    protected $logger;
    protected $handler;
    protected $sourceDir;

    public function __construct(LoggerInterface $logger, HandlerInterface $handler)
    {
        parent::__construct();

        $this->logger = $logger;
        $this->handler = $handler;
    }

    protected function configure()
    {
        $this
            ->setName('generate')
            ->setDescription('Generate variants for the given file')
            ->addArgument('source', InputArgument::REQUIRED, 'Source file or folder', null)
            ->addArgument('destination', InputArgument::REQUIRED, 'Output folder', null)
            ->addOption('overwrite', false, InputOption::VALUE_NONE, 'Always overwrite variants, even if newer than source')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->info('Starting up');

        $this->handler->setOutputHandler(new Filesystem(new Adapter($input->getArgument('destination'))));

        $source = realpath($input->getArgument('source'));
        $overwrite = $input->getOption('overwrite');

        if (!$source) {
            throw new \InvalidArgumentException('Source file or folder does not exist.');
        }

        if (is_dir($source)) {
            $this->logger->info(sprintf('Processing directory %s', $source));

            $input = new Filesystem(new Adapter($source));
            $this->handler->setInputHandler($input);

            foreach ($input->listContents() as $file) {
                if ($file['type'] !== 'file') {
                    continue;
                }

                try {
                    $this->handler->buildVariants($file['path'], $overwrite);
                } catch (\InvalidArgumentException $e) {
                    $this->logger->error(sprintf('Skipping unprocessable input file %s: %s', $file['path'], $e->getMessage()));
                }
            }

            return;
        }

        $info = pathinfo($source);
        $this->handler->setInputHandler(new Filesystem(new Adapter($info['dirname'])));
        $this->handler->buildVariants($info['basename']);
    }

}