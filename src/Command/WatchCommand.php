<?php
namespace Tui\Rusty\Command;

use League\Flysystem\Adapter\Local as Adapter;
use League\Flysystem\Filesystem;
use Lurker\Event\FilesystemEvent;
use Lurker\ResourceWatcher;
use Lurker\Resource\FileResource;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tui\Rusty\Handler\HandlerInterface;

class WatchCommand extends Command
{
    protected $handler;
    protected $logger;
    protected $watcher;

    public function __construct(LoggerInterface $logger, ResourceWatcher $watcher, HandlerInterface $handler)
    {
        parent::__construct();

        $this->logger = $logger;
        $this->watcher = $watcher;
        $this->handler = $handler;
    }

    protected function configure()
    {
        $this
            ->setName('watch')
            ->setDescription('Watch a source folder and create/update/delete variants accordingly')
            ->addArgument('source', InputArgument::REQUIRED, 'Source folder', null)
            ->addArgument('destination', InputArgument::REQUIRED, 'Output folder', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->info('Starting up');

        $source = realpath($input->getArgument('source'));
        $destination = $input->getArgument('destination');

        if (!$source || !is_dir($source)) {
            throw new \InvalidArgumentException('Source does not exist or isn\'t a folder.');
        }

        if (!$destination || !is_dir($destination)) {
            throw new \InvalidArgumentException('Destination does not exist or isn\'t a folder.');
        }

        $this->handler->setInputHandler(new Filesystem(new Adapter($source)));
        $this->handler->setOutputHandler(new Filesystem(new Adapter($destination)));

        $this->configureWatcher($source);
        $this->watcher->start();
    }

    protected function configureWatcher($source)
    {
        $events = [
            ['name' => 'rusty.sources_add', 'path' => $source, 'type' => FilesystemEvent::CREATE],
            ['name' => 'rusty.sources_edit', 'path' => $source, 'type' => FilesystemEvent::MODIFY],
            ['name' => 'rusty.sources_delete', 'path' => $source, 'type' => FilesystemEvent::DELETE],
        ];

        foreach ($events as $event) {
            $this->watcher->track($event['name'], $event['path'], $event['type']);
        }

        $listeners = [
            'rusty.sources_add' => function (FilesystemEvent $event) use ($source) {
                if (!$event->getResource() instanceof FileResource) {
                    return;
                }

                $imagePath = substr($event->getResource(), strlen($source));
                $this->logger->info('New file ' . $imagePath);

                try {
                    $this->handler->buildVariants($imagePath);
                } catch (\InvalidArgumentException $e) {
                    $this->logger->error(sprintf('Skipping unprocessable input file %s: %s', $imagePath, $e->getMessage()));
                }
            },
            'rusty.sources_edit' => function (FilesystemEvent $event) use ($source) {
                if (!$event->getResource() instanceof FileResource) {
                    return;
                }

                $imagePath = substr($event->getResource(), strlen($source));
                $this->logger->info('File modified ' . $imagePath);

                try {
                    $this->handler->buildVariants($imagePath);
                } catch (\InvalidArgumentException $e) {
                    $this->logger->error(sprintf('Skipping unprocessable input file %s: %s', $imagePath, $e->getMessage()));
                }
            },
            'rusty.sources_delete' => function (FilesystemEvent $event) use ($source) {
                if (!$event->getResource() instanceof FileResource) {
                    return;
                }

                $imagePath = substr($event->getResource(), strlen($source));
                $this->logger->info('File removed ' . $imagePath);
                $this->handler->deleteVariants($imagePath);
            },
        ];

        foreach ($listeners as $event => $callback) {
            $this->watcher->addListener($event, $callback);
        }
    }

}