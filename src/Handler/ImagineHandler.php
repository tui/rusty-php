<?php
namespace Tui\Rusty\Handler;

use InvalidArgumentException;
use Imagine\Image\ImagineInterface;
use League\Flysystem\FilesystemInterface;

class ImagineHandler extends AbstractHandler
{
    protected $imagine;

    public function __construct(ImagineInterface $imagine)
    {
        $this->imagine = $imagine;
    }

    protected function generateVariant($inputImage, $outputFilename, array $config) {
        $this->logger->debug('Creating image from inputHandler stream');
        $image = $this->imagine->read($this->inputHandler->readStream($inputImage));

        $mimetype = $this->inputHandler->getMimetype($inputImage);
        $type = substr($mimetype, strpos($mimetype, '/') + 1);
        if (!in_array($type, ['jpeg', 'gif', 'png'])) {
            throw new InvalidArgumentException(sprintf('Unexpected mime-type %s', $mimetype));
        }

        $newImage = $image->copy();
        $newImage->resize($image->getSize()->widen($config['width']));

        $this->logger->debug(sprintf('Writing (type: %s) to %s', $type, $outputFilename));
        $this->outputHandler->put($outputFilename, $newImage->get($type, [
            'quality' => $config['quality'],
        ]), [
            'visibility' => 'public',
            'mimetype' => $mimetype,
        ]);
    }
}