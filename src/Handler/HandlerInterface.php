<?php
namespace Tui\Rusty\Handler;

use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;

/**
 * Handlers are responsible for building, updating, and deleting image variants.
 * The interface uses setter injection for configuration and required objects in
 * order to keep the constructer clear for your own uses.
 */
interface HandlerInterface
{
    /**
     * Set image variants
     * 
     * e.g. $handler->setVariants([
     *     'small'  => ['width' => 320, 'quality' => 80],
     *     'medium' => ['width' => 640, 'quality' => 80],
     *     'large'  => ['width' => 960, 'quality' => 80],
     *     'huge'   => ['width' => 1440, 'quality' => 80],
     * ]);
     * 
     * @var array $variants List of image variants
     * @return null
     */
    public function setVariants(array $variants);

    /**
     * Set input handler
     * 
     * Uses League's Flysystem abstraction library to allow multiple output sources
     * 
     * @var \League\Flysystem\FilesystemInterface $handler Filesystem handler
     */
    public function setInputHandler(FilesystemInterface $handler);

    /**
     * Set output handler
     * 
     * Uses League's Flysystem abstraction library to allow multiple output sources
     * 
     * @var \League\Flysystem\FilesystemInterface $handler Filesystem handler
     */
    public function setOutputHandler(FilesystemInterface $handler);

    /**
     * Create and write variants of the input image according to the configured variants
     * 
     * @var string $inputImage path to input image - should be relative to the source directory
     * @return null
     */
    public function buildVariants($inputImage);

    /**
     * Remove variants of the input image according to the configured variants
     * 
     * @var string $inputImage path to input image
     * @return null
     */
    public function deleteVariants($inputImage);

    /**
     * Set logger
     * 
     * NOTE: the logger interface includes a null logger if you don't want to log
     * 
     * @var \Psr\Log\LoggerInterface $logger
     * @return null
     */
    public function setLogger(LoggerInterface $logger);
}