<?php
namespace Tui\Rusty\Handler;

use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractHandler implements HandlerInterface
{
    protected $variants;
    protected $inputHandler;
    protected $outputHandler;
    protected $logger;

    public function setVariants(array $variants)
    {
        $this->variants = $variants;
    }

    public function setInputHandler(FilesystemInterface $handler)
    {
        $this->inputHandler = $handler;
    }

    public function setOutputHandler(FilesystemInterface $handler)
    {
        $this->outputHandler = $handler;
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function deleteVariants($inputImage)
    {
        $this->logger->info(sprintf('Removing variants of file %s', $inputImage));
        foreach ($this->variants as $name => $config) {
            $this->logger->debug(sprintf('Removing variant %s', $name), $config);
            $filename = $this->getVariantFilename($inputImage, $name);

            try {
                $this->outputHandler->delete($filename);
            } catch (\League\Flysystem\FileNotFoundException $e) {
                $this->logger->debug(sprintf('Variant doesn\'t exist: %s', $name), $config);
            }
        }
    }

    public function getVariantFilename($inputImage, $variantName)
    {
        $info = pathinfo($inputImage);

        if ($variantName) {
            return vsprintf('%s/%s,%s.%s', [
                $info['dirname'] === '/' ? '' : $info['dirname'],
                $info['filename'],
                $variantName,
                $info['extension'],
            ]);
        }

        return $inputImage; // No variant name? Just use original filename
    }

    public function buildVariants($inputImage, $overwriteNewer = false)
    {
        $this->logger->info(sprintf('Building variants of file %s', $inputImage));

        // Get input file date
        $inputTimestamp = $this->inputHandler->getTimestamp($inputImage);

        // Generate variants
        foreach ($this->variants as $name => $config) {
            $this->logger->debug(sprintf('Generating %s variant', $name ?: 'default'), $config);

            $filename = $this->getVariantFilename($inputImage, $name);

            // Check whether output exists and whether we should overwrite it anyway
            if (!$overwriteNewer 
                && $this->outputHandler->has($filename)
                && ($variantTimestamp = $this->outputHandler->getTimestamp($filename))
                && $variantTimestamp > $inputTimestamp
            ) {
                $this->logger->info(vsprintf('%s variant of file %s is newer than source, skipping.', [
                    $name ?: 'default',
                    $inputImage,
                ]));
                continue;
            }

            // Create new variant
            $this->generateVariant($inputImage, $filename, $config);
        }
    }

    /**
     * Create a variant of the given image
     *
     * @param string $inputImage Path of input image (to pass to e.g. $this->inputHandler->readStream($inputImage))
     * @param string $outputFilename Path to write output image (e.g. $this->outputHandler->put($outputFilename,…))
     * @param string $config Variant configuration (size, quality)
     *
     * @return null
     */
    abstract protected function generateVariant($inputImage, $outputFilename, array $config);
}