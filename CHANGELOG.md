# Changelog

## 0.3.2 – 2014-02-13

* Default variants are now full retina (2x) at 80% quality.
* Fixed a file-type test in `generate` command
* AbstractHandler now features a generic `buildVariants` method
* Variants which are newer than the source image won't be overwritten. You can force it on the `generate` command with the new `--overwrite` option.

## 0.3.1 – 2014-02-10

* `watch` and `generate` with a source folder now skip non-image files, instead of exploding.
* Sub-folder structure in source is now correctly translated to destination.

## 0.3.0 – 2014-02-10

* Generate and watch commands now both take source and destination arguments (no longer hardcoded into config)
* If you supply a folder name instead of a filename as the source, then all files are processed

## 0.2.2 – 2014-02-07

* Don't explode if a variant doesn't exist during a delete
* Allow a variant to have a blank name - in which case use the original filename

## 0.2.1 – 2014-02-03

* If I actually added the GenerateCommand file, that would probably help.

## 0.2.0 – 2014-02-03

* New 'generate' command for generating variants for a single image

## 0.1.1 – 2014-02-03

* Fix variant filenames (incorrectly included file extension twice)

## 0.1.0 – 2014-02-02

* Initial proof-of-concept release.
