# Rusty-php

Rusty-php is a PHP implementation of an image recompression utility for creating and managing resized image variants for use with the rusty-js responsive image script. 

## Installation

```
composer install -o # Install dependencies and optimise autoloader
```

## Usage

To simply generate the image variants for a single image, use the `generate` command:

```
bin/rusty generate web/images/sources/example.jpg web/images/
```

If you set a directory as the source, instead of a specific image, Rusty will try to generate variants for all files in that folder.

During development, it can be useful to watch a source folder and automatically generate or delete variants when sources are added or removed:

```
bin/rusty watch -vv web/images/sources web/images
```

Both commands report only errors by default; use the `-vv` option to view info, and `-vvv` to show debug output. You can quash output by passing `--quiet`.


## Known issues

* Both `watch` and `generate` will always overwrite variants, even if they're newer than the source image.

## Configuration

Rusty's really early in development right now (no configuration mechanism), so you'll have to edit `src/container.php` and override or extend the services defined there. Sorry about that… feel free to send a pull request though!

### Image handler

The image handler builds and manages the image variants. The default implementation uses the Imagine library, using the IMagick, GMagick, or GD extensions (in that order of preference). You can also make your own handler (for example a ResqueHandler which queues compression… hint hint)


### Changing output variant names or sizes

Replace or extend `$di['image.sizes']`. The default looks like this:

```php
$di['image.sizes'] = [
    'small'  => ['width' => 320*1.5, 'quality' => 80],
    'medium' => ['width' => 640*1.5, 'quality' => 80],
    'large'  => ['width' => 960*1.5, 'quality' => 80],
    'huge'   => ['width' => 1440*1.5, 'quality' => 80],
];
```
